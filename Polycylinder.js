class Polycylinder{
	//object to hold each polycylinder tree data
	constructor(){
	    this.tree = [];
		this.NumVertices = 0;
		this.NumIndices = 0;
	}
	
	combine(c, s)
	{
	    this.tree.push([c, s]);
		this.NumVertices = this.NumVertices + c.getNumVertices() + s.getNumVertices();
		this.NumIndices = this.NumIndices + c.getNumIndices() + s.getNumIndices();
	}
	
	getNumIndices(){ return this.NumIndices; }
}

class Cylinder{
//The code was provided in class
    constructor(height, radius, X, Y, Z){
		this.cylinderIndex = 0;
		this.cylinderVertices = 0;
		
		//used to link for polycylinder
		this.bot_loc = vec3(X, Y, Z);
		this.top_loc = vec3(X, Y + height, Z);
		var height = 0.1;
		var x_bot, y_bot, z_bot, x_top, y_top, z_top;
	    for (var i = 0; i < 2*Math.PI+.1; i+=0.1)
		{
			//find bot circle vertices
		    x_bot = radius * Math.cos(i);
			z_bot = radius * Math.sin(i);
			y_bot = Y;
			
			pointsArray.push(vec4(x_bot+X, y_bot, z_bot+Z, 1.0));
			
			//find top circle vertices
			x_top = radius * Math.cos(i);
			z_top = radius * Math.sin(i);
			y_top = height + Y;
			
			pointsArray.push(vec4(x_top+X, y_top, z_top+Z, 1.0));
			this.cylinderVertices += 2;
			totalVertices += 2;
		}
	}
	//The code was provided in class
	plotCylinder(NumVertices, set)
	{
		for (var i = set; i < NumVertices+set; i++)
		{
			//four indices of mesh "block"
			var vA = i;
			var vB = i+1;
			var vC = i+2;
			var vD = i+3;
			
			//two triangles of square face used to draw 
			indexBuff.push(vA);
			indexBuff.push(vB);
			indexBuff.push(vD);
			indexBuff.push(vA);
			indexBuff.push(vC);
		    indexBuff.push(vD);
			
			this.cylinderIndex+=6;
		}
	}
	
	getNumVertices(){ return this.cylinderVertices; }
	
	getNumIndices(){ return this.cylinderIndex; }
	
	getLinks(){ return [this.bot_loc, this.top_loc] }
}

class Sphere{
    constructor(radius, latBands, lonBands, X, Y, Z)
    {	
	    this.sphereVertices = 0;
		this.sphereIndices = 0;
		//The code was provided in class
		//rotate arround latitude and longitude and plot circles
        for (var latNum= 0; latNum< latBands + 1; ++latNum)
	    {
			var theta = latNum* Math.PI/latBands; //0 to pi
			var z = radius * Math.cos(theta); //distance from center
			
		    for (var lonNum= 0; lonNum< lonBands + 1; ++lonNum)
			{
				var phi = 2 * lonNum* Math.PI/lonBands;//0 to 2pi
				
				//circle points at current rotation
				var u = Math.sqrt((radius * radius) - (z * z));
			    var x = u * Math.cos(phi);
			    var y = u * Math.sin(phi);
			
			    pointsArray.push(vec4((X+x), (Y+y), (Z+z), 1.0));
				
				this.sphereVertices++;
				totalVertices++;
			}
	    }
    }
    //The code was provided in class
	plotSphere(latBands, lonBands, set)
	{
		//indexBuff.length = 0;
		for (var latNum= 0; latNum< latBands; ++latNum)
		{
			for (var lonNum= 0; lonNum< lonBands; ++lonNum)
			{
				//find four vertices of mesh "block"
				var vA = latNum* (lonBands + 1) + lonNum+ set;
				var vB = vA + (latBands + 1);
				var vC = vA + 1;
				var vD = vB + 1;
				
				//push the indices of the two triangles that make up the face
				indexBuff.push(vA);
				indexBuff.push(vB);
				indexBuff.push(vC);
				indexBuff.push(vB);
				indexBuff.push(vD);
				indexBuff.push(vC);
				
				this.sphereIndices += 6;
			}
		}
	}
	
	getNumVertices(){ return this.sphereVertices; }	
	getNumIndices() { return this.sphereIndices; }
}