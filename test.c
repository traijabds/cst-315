//Dinh Tran
//CST 315
//01/29/18
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>


int top;
int buffer[10];

//function to put value into a buffer
bool put(int x)
{
    if (top >= 10)
    {
        printf ("buffer is full");
        return false;
    }
    else
    {
        top++;
	buffer[top]=x;        
	return true;
    }
}

//function to get value from buffer
int get()
{
    if (top < 0){
        printf ("buffer is empty");
        return 0;
    }
    else
    {
	int x = buffer[top];
        top--;
        return x;
    }
}
int product=0;

//function to produce 
int produce(){
	return product++;
}

//function to consume
void consume(int i){
printf("%i",i);
}

//function producer
void producer(){

 printf("Running Producer Process.....\n");
	int i;	
//keep produce untill the buffer is full
	while (put(produce()))
	{
	
	}
}

//function consumer
void consumer(){

    printf("Running Consumer Process.....\n");
//consume untill the buffer is empty	
	while (get()!=0)
	{
		consume(get());
	}
}




int main(int argc, char* argv[]){



// call the function producer and consumer.
		
	producer();
        consumer();
   

    // Return 
    return 0;

}
