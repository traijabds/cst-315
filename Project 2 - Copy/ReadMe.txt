Dinh Minh Tran & Josiah Jibben
CST 315
04/13/2018
Project 2: a Virtual Memory Manager
Professor: Luke Kanuchok

Description:
 - In the project, we will define, explain, design, and write code for a few building blocks of our VMM algorithm. 
 - To complete this project, we need to learn about the concepts of: Memory allocation, virtual addresses, page tables and page frames.
 - Manage memory resources.
 - Design, implement, and manage page tables and frames.
 - Implement paging algorithms.

Software:
 - CLion to run the C++ program
 - Ubuntu to run C++ program in Linux OS
 - Microsoft Word for document file
 - Notepad++ for Readme file.
 
Instruction:
 - Open Oracle VM VirtualBox and run Ubuntu to work on the Linux OS
 - Use Linux for C++ program, run the program using g++
 - Input number if required for number of processes and resources.
 