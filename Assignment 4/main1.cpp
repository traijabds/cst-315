#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
int main(int argc, char** argv) {
    char buff[15];
    if(argc<2){
        fprintf(stderr,"USAGE: %s string\n",argv[0]);
        return 1;
    }
    strcpy(buff,argv[1]);
    return 0;
}
//This program have a common problem in buffer overflow when the string is too long and leak out into other buffers.
//In this case, if the string is 14 long, it will be fine, but it always is a problem if the string is greater than or
//equal 15. It happened due to this string was written into this buffer without checking its length(size)
// Fortunately, this program will run well without segmentation fault.