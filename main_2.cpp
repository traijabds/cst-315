/* Authors: Josiah Jibben, Dinh Tran
 * Course: CST-315
 * Date: 2/7/18
 * Command Line interpreter: This program will accept input from a user to execute through the command line using
 * multiple processes.
 */

#include <iostream>
#include <vector>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fstream>
#include <sstream>

using namespace std;

void readfile();
void execCommands();
vector<string> temp;
vector<string> commands;
string delimiter = ";";

int main(int argc, char *argv[]) {
    string input;

    int prompt;

    cout << "Enter [1] to read commands from a file." << endl;
    cout << "Enter [2] to manually enter commands." << endl;
    cin >> prompt;
    if(prompt == 1) {
        readfile();
        execCommands();
    }
    else if(prompt == 2) {
        // Continually accept commands until exit is called.
        while (true) {

            // Empty the vector of strings for commands.
            commands.clear();

            // Prompt the user for an input command. Break if user chooses to exit.
            cout << "CMDLINE: ";
            cin >> input;
            if (input == "exit") {
                cout << "Exiting command line." << endl;
                break;
            }

            // Allow multiple commands to be entered when separated by semicolons.
            size_t pos = 0;
            string temp;
            while ((pos = input.find(delimiter)) != string::npos) {
                temp = input.substr(0, pos);
                // Push the command up to the delimiter into a vector of commands. Ignore exit commands.
                if(temp != "exit") {
                    commands.push_back(temp);
                }
                // Erase everything before the delimiter in the string.
                input.erase(0, pos + delimiter.length());
            }
            commands.push_back(input);

            // Uncomment this block to see all the commands entered.
/*
        cout << "The following commands will be run simultaneously:" << endl;
        for(int i = 0; i < commands.size(); i++){
            cout << commands.at(i) << endl;
        }
*/

            execCommands();

            // End of while loop, return to prompt
        }
    }
    else{
        cout << "Choice is invalid." << endl;
    }

    // Program has ended.
    return 0;
}


// Read commands from a file
void readfile()
{
    // Prepare file to be read.
    ifstream infile;
    infile.open("testa.txt");
    string line;

    // Empty the vector of strings for commands.
    commands.clear();

    // Check if file opens correctly.
    if (infile.fail())
    {
        cout << "Could not read file. Check that testa.txt is within the same folder as this project." << endl;
    }

    else
    {
        cout << "Commands retrieved from file:" << endl;
        // The first set of data does not contain parentheses, so it is parsed differently than the rest of the file.

        // Empty the vector so that it can be manipulated.
        temp.clear();

        // Variables for the file to read into.
        string val;
        string delimiter = ";";

        while(getline(infile,line)) {
            size_t position = 0;
            string command;
            while ((position = line.find(delimiter)) != string::npos) {
                command = line.substr(0, position);
                if(command != "exit") {
                    commands.push_back(command);
                    cout << command << endl;
                }
                line.erase(0, position + delimiter.length());
            }
        }
    }

    // Close instruction file, end of function.
    infile.close();
}

void execCommands(){
    // Create a char* array of the commands in the vector, as this is the required format for exec.
    int lenOfArr = commands.size();
    char *com[lenOfArr];

    int index = 0;
    for (int i = 0; i < lenOfArr; i++) {
        // Move all commands from the vector to the array.
        com[index] = (char *) commands.at(i).c_str();
        index++;

        // End of each command must be NULL for the exec() command to function. This allows only one array to be used.
        com[index] = NULL;
        index++;
    }

    // Create a child process using fork()
    // Execute command(s) from user
    pid_t pid = fork();
    if (pid == 0) {
        // Child process
        if (execvp(com[0], com) == -1) // Check if execution of command fails.
        {
            cout << "Execution failed." << endl;
        }
    } else if (pid > 0) {
        // Parent process
        if (commands.size() > 1) // Parent process only executes if multiple commands are entered.
        {
            if (execvp(com[2], com) == -1) // Run the command in the array after a NULL.
            {
                cout << "Execution failed." << endl;
            }
        } else {
            // Do nothing.
        }

        wait(0); // Wait for child process to finish.
    } else {
        cout << "Forking failed." << endl;
    }

}