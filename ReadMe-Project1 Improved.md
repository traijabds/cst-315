Dinh Minh Tran and Josiah Jibben
CST 315
02/26/2018
Professor: Glenn Williamson
Project 1: Improved Unix/Linux Command Line Interpreter


- Constructions from running:
• Firstly, we will prompt the user for an input commands, 
	so that we can create a shell to direct that operation of computer.
		Execute Shell Script using file name.
		Execute Shell Script by specifying the Interpreter
• By creating the functions execv() and fork(), we can apply child process to execute the command entered by the user.
		Execute Shell Script using “. ./”
		Using Source Command
• If the commands are entered are more than one or multiple commands, we will use the function wait() and waitpid() for executing.
		Exit the shell
		Define the combination of keys for the functions listed above.

	
- Software requirement:
	+ We need to open Virtual Machine and run the Project1.c program
	+ The c file can run online through the GDB online Debugger tool (https://www.onlinegdb.com/)
	
- Features with instructions: This program will accept input from a user to execute through the command line using multiple processes.
	It will continue accept commands until exit is called.
	First it will empty the vector of strings for commands, then prompt the users for an input command. Break if user chooses to exit.
	It also allows multiple commands to be entered when separated by semicolons.
	Push the commands up to the delimiter into a vector of commands.
	This program also reads commands from files, then manipulate it.
