Dinh Minh Tran
CST 310
03/22/2018
Professor: Jeffrey Griffith
Project 6


Instruction:
- Open the sublime text 3 to open the .html and .js file to generate the code and rendering the terrain CandyLand and pinning trees onto terrain using polyCylinder
- When done with generating the code, open the .html by Google Chrome to see the display objects through the WebGL code.
- In case of any trouble, cannot see the scene or getting some errors, right click on Google Chrome and going to inspect -> Console, 
it will show up the error, syntax or any mistake on the logics.

Software used of requirement:
- Sublime Text 3 to code the .html and .js files.
- Google Chrome used to open and display the scene through the .html and .js file.

Files and Documents:
- Project6.docx
- Terrain.html
- CandyLand.js
- Polycylinder.js
- Screenshot
- ReadMe.txt