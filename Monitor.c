#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

//Reference: Textbook (Mutex part) and programs from CST 221
pthread_mutex_t mymutex;
void *printOdd(void *arg)
{
	pthread_mutex_lock(&mymutex);
	int  i =0;
	for(i=0;i<100;i++)
		{
			printf("Receiption %d",i);
		}
	pthread_mutex_unlock(&mymutex);
pthread_exit(NULL);
}
void *printEven(void *arg)
{
	pthread_mutex_lock(&mymutex);
	int  i =1;
	for(i=1;i<100;i++)
		{
			printf("Receiption %d",i);
		}
	pthread_mutex_unlock(&mymutex);
pthread_exit(NULL);
}
int main ()
{
	pthread_t tid1, tid2;
	pthread_mutex_init(&mymutex,NULL);
	pthread_create(&tid1, NULL, printOdd, NULL);
	pthread_create(&tid2, NULL, printEven, NULL);
	pthread_join(tid1,NULL);	
	pthread_join(tid2,NULL);
	pthread_mutex_destroy(&mymutex);
	pthread_exit(NULL);
}