var canvas;
var gl;

var pointsArray  = [];
var normalsArray = [];

var program;

var near   = -10;
var far    = 10;
var radius = 1.0;
var theta  = 2.5;
var phi    = 2.3;
var dr     = 5.0 * Math.PI/180.0;

var eye  = vec3(1.0, 1.0, 1.0);
const at = vec3(0.0, 0.0, 0.0);
const up = vec3(0.0, 1.0, 0.0);

var left   =-2.0;
var right  = 2.0;
var ytop   = 2.0;
var bottom =-2.0;

var lightPosition = vec4( 0.0, 1.0, 0.0, 0.0 );
var lightAmbient  = vec4( 0.2, 0.2, 0.2, 1.0 );
var lightDiffuse  = vec4( 1.0, 1.0, 1.0, 1.0 );
var lightSpecular = vec4( 0.0, 0.0, 1.0, 1.0 );

var materialAmbient   = vec4( 0.0, 0.0, 0.0, 0.0 );
var materialDiffuse   = vec4( 0.0, 0.8, 0.0, 0.0 );
var materialSpecular  = vec4( 0.0, 0.0, 0.0, 0.0 );
var materialShininess = 100.0;

var ambientProduct  = mult(lightAmbient, materialAmbient);
var diffuseProduct  = mult(lightDiffuse, materialDiffuse);
var specularProduct = mult(lightSpecular, materialSpecular);

var modelViewMatrix, projectionMatrix;
var modelViewMatrixLoc, projectionMatrixLoc;
var nMatrix, nMatrixLoc;
var eye;

var trees         = [];
var numTrees      = 55;
var totalVertices = 0;

var indexBuff = [];
var iBuffer;

var terrVertices = [];
var terrIndices  = 0;

var xTerrain = 8;
var zTerrain = 8;



function generateTerrain()
{
	//grid x length centered at origin (if 8 move from -4 to 4)
    for (var i = Math.floor(-xTerrain/2); i < (Math.floor(xTerrain/2))+1; i++)
	{//grid z length centered at origin
	    for (var j = Math.floor(-zTerrain/2); j < (Math.floor(zTerrain/2))+1; j++)
		{
			//scale plane to view size coords
		    var x = i/xTerrain*3;
			var z = j/zTerrain*3;
			
			//create a point that is close to others (also randomized) based on cosine value
			var y = 1.25 * Math.cos(z) * Math.cos(x) * Math.random();
			
			//push to array for drawing
			pointsArray.push(vec4(x, y, z, 1.0));			
			totalVertices++;
		}
	}
}

function plotTerrain()
{
    for (var i = 0; i < xTerrain; ++i)
	{
	    for (var j = 0; j < zTerrain; ++j)
		{
			//find six indices of two triangles for each "block" of mesh to be drawn
		    var vA = i * (xTerrain+1) + j;
			var vB = vA + (zTerrain+1);
			var vC = vA + 1;
			var vD = vB + 1;
			
			indexBuff.push(vA);
			indexBuff.push(vB);
			indexBuff.push(vD);
			indexBuff.push(vA);
			indexBuff.push(vC);
			indexBuff.push(vD);
			
			terrIndices+=6;
		}
	}
}

function pinTrees()
{
	//make a copy of terrain vertices to offset the drawing of the trees
    terrVertices = pointsArray.slice();
	
	for (var i = 0; i < numTrees; ++i)
	{
		//create a new tree and generate at random location
		var polyCylinders = new Polycylinder();
		generateTree(terrVertices[Math.floor(Math.random() * terrVertices.length)], 4, polyCylinders);		
		trees.push(polyCylinders);
	}
}

function generateTree(root, n, polyCylinders)
{
	var x = root[0];
    var y = root[1];
    var z = root[2];
	
	if (n > 0){
		//create a cylinder and add a sphere to the top
		var c = new Cylinder(.05, .01, x, y, z);
		c.plotCylinder( c.getNumVertices(), totalVertices - c.getNumVertices());

		var joint = c.getLinks()[1];
		var x = joint[0];
		var y = joint[1];
		var z = joint[2];
		
		var s = new Sphere(.01, 5, 5, x, y, z);
		s.plotSphere(5, 5, totalVertices - s.getNumVertices());
		
		//add to polycylinder object (unused for now)
		polyCylinders.combine(c, s);
		
		//recursively generate additional branches
		generateTree(joint, n - 1, polyCylinders);
	}
}

window.onload = function init()
{
	//create a canvas
    canvas = document.getElementById( "gl-canvas" );

	//ensure no errors
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
	
	//view the canvas
    gl.viewport( 0, 0, canvas.width, canvas.height );

	//light blue sky
    gl.clearColor( 0.01, 0.5, 1.0, 0.2 );

	//create 3D effect
	gl.enable(gl.DEPTH_TEST);
	
	//creatting terrains
	generateTerrain();
	plotTerrain();
	
	//drag trees into random locations
	pinTrees();
	
	//vertex/fragment shaders
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

	//buffers for shaders
	//colors
	var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(normalsArray), gl.STATIC_DRAW );

    //vertices
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(pointsArray), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
	
	//indices of terrain and polycylinders meshes
	iBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexBuff), gl.STATIC_DRAW);

	//camera
	//matrices for viewing model
    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
	
	//buttons for camera rotation around terrain
	document.getElementById("Button0").onchange = function(event)
	{
    	theta = event.target.value;
	};

    document.getElementById("Button1").onchange = function(event) 
    {
    	phi = event.target.value;
	};

	//render the scene
    render();
}


var render = function() {
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
	//update camera
	eye = vec3(radius*Math.sin(theta)*Math.cos(phi),
               radius*Math.sin(theta)*Math.sin(phi),
			   radius*Math.cos(theta));
    modelViewMatrix = lookAt(eye, at , up);
    projectionMatrix = ortho(left, right, bottom, ytop, near, far);
	                         
    gl.uniformMatrix4fv( modelViewMatrixLoc, false, flatten(modelViewMatrix) );
    gl.uniformMatrix4fv( projectionMatrixLoc, false, flatten(projectionMatrix) );

    // render mountain and tree trunks
	for (var i = 0; i < xTerrain; i++){
        gl.drawElements( gl.TRIANGLES, terrIndices/xTerrain, gl.UNSIGNED_SHORT, 2 * i * terrIndices/xTerrain );
	}	
	for (var i = 0; i < trees.length; i++){
        gl.drawElements( gl.TRIANGLES, trees[i].getNumIndices(), gl.UNSIGNED_SHORT, 2 * ( terrIndices + (i * trees[i].getNumIndices()) ) );
	}
    requestAnimFrame(render);
}

  