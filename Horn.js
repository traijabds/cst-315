"use strict";
//Initializes variables.
var canvas;
var gl;

var NumVertices  = 0;  // (2 triangles per face or 6 vertices per face) for 6 faces

var points = [];
var normalsArray =[];
var colors = [];

var xAxis = 0;
var yAxis = 1;
var zAxis = 2;

var axis = 0;
var theta = [ 0, 0, 0 ];

var thetaLoc;

var near = 1.0;
var far = 100;
var fovy = 80.0;
var aspect;
var VMatrixLoc;
var PMatrixLoc;
var vs_ViewMatrix;
var vs_ProjMatrix;
var vs_Dist = [0.0, 0.0, 0.0];
var vs_Theta = [0.0, 0.0, 0.0];
var vsDistLoc;
var Dx, Dy, Dz = 0.0;
var freeze = true;
var walk = true;

var eyePt;
var atPt = vec3(0.0, 0.0, -100);
var upVec = vec3(0.0, 1.0, 0.0);
eyePt = vec3(0, 0, 5);


var lightPosition = vec4(-5.0, 4.0, 2.0, 0.0 );
var lightAmbient = vec4(0.2, 0.2, 0.2, 1.0 );
var lightDiffuse = vec4( 1.0, 1.0, 1.0, 1.0 );
var lightSpecular = vec4( 1.0, 1.0, 1.0, 1.0 );

var materialAmbient = vec4( 1.0, 0.0, 1.0, 1.0 );
var materialDiffuse = vec4( 1.0, 0.8, 0.0, 1.0);
var materialSpecular = vec4( 1.0, 1.0, 1.0, 1.0 );
var materialShininess = 100.0;


var eyeX = -0.2;
var eyeY = 0.5;
var eyeZ = 5.0;
var centerX = 0.0;
var centerY = 0.0;
var centerZ = -180;
var upX = 0.0;
var upY = 1.0;
var upZ = 0.0;
var program;

var ctm;
var ambientColor, diffuseColor, specularColor;
var modelView, projection;
var viewerPos;

var modelViewMatrix, projectionMatrix;
var modelViewMatrixLoc, projectionMatrixLoc;
var eyeLoc;




var flag = true;

window.onload = function init()
{
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.0, 0.3, 0.5, 0.5 );

	// Activates depth comparisons and updates to the depth buffer
	
    gl.enable(gl.DEPTH_TEST);     

    //
    //  Apply shader into the program
    //
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    buildCube();

    //Loading vertices into program
	loadVertices(program);
	
    //Loading color into cube faces
	loadColors(program);

    // get the location of the angle

    thetaLoc = gl.getUniformLocation(program, "theta");

    //Using Phong model for lighting view
    loadPhongModel();

    //event listeners for buttons

    
    document.getElementById("fieldOfView").onchange = function(){
        fovy = event.srcElement.value;
    };
    document.getElementById("eyeX").onchange = function(){
        eyeX = event.srcElement.value;
    };
    document.getElementById("eyeY").onchange = function(){
        eyeY = event.srcElement.value;
    };
    document.getElementById("eyeZ").onchange = function(){
        eyeZ = event.srcElement.value;
    };
    document.getElementById("centerX").onchange = function(){
        centerX = event.srcElement.value;
    };
    document.getElementById("centerY").onchange = function(){
        centerY = event.srcElement.value;
    };
    document.getElementById("centerZ").onchange = function(){
        centerZ = event.srcElement.value;
    };
    document.getElementById("upX").onchange = function(){
        upX = event.srcElement.value;
    };
    document.getElementById("upY").onchange = function(){
        upY = event.srcElement.value;
    };
    document.getElementById("upZ").onchange = function(){
        upZ = event.srcElement.value;
    };


    render();
}

function loadVertices(program) {

    var nBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(normalsArray), gl.STATIC_DRAW );
    
    var vs_Normal = gl.getAttribLocation( program, "vs_Normal" );
    gl.vertexAttribPointer( vs_Normal, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vs_Normal );
    // Load the vertices for the triangles and enable the attribute vPosition

    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );
    
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

}


function loadColors(program) {

    // Load the colors for the triangles and enable the attribute vColor
   
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
}


function buildCube() {

	// Build all 6 faces of the cube
	
    buildFace( 1, 0, 3, 2 );    // Cube Face 1
    buildFace( 2, 3, 7, 6 );    // Cube Face 2
    buildFace( 3, 0, 4, 7 );    // Cube Face 3
    buildFace( 6, 5, 1, 2 );    // Cube Face 4
    buildFace( 4, 5, 6, 7 );    // Cube Face 5 
    buildFace( 5, 4, 0, 1 );    // Cube Face 6 

    buildFace( 9,  8,  11, 10 );      // Cube Face 7
    buildFace( 10, 11, 15, 14 );    // Cube Face 8
    buildFace( 11, 8,  12, 15 );     // Cube Face 9
    buildFace( 14, 13, 9,  10 );     // Cube Face 10
    buildFace( 12, 13, 14, 15 );    // Cube Face 11
    buildFace( 13, 12, 8,  9 );      // Cube Face 12 


//road
    buildFace(17, 16, 18, 19);
    buildFace(20, 21, 22, 23);
    buildFace(24, 25, 26, 27);

//Windows
    buildFace(28, 29, 31, 30);
    buildFace(32, 33, 35, 34);
    buildFace(29, 31, 33, 35);
    buildFace(28, 30, 32, 34);
    buildFace(28, 29, 35, 34);
    buildFace(30, 31, 32, 33);
//Wheel right outer
    buildFace(38, 39, 37, 36);
    buildFace(40, 41, 43, 42);
    buildFace(37, 39, 41, 43);
    buildFace(36, 38, 41, 42);
    buildFace(36, 37, 43, 42);
    buildFace(38, 39, 41, 40);
//Wheel left outer
    buildFace(44, 45, 47, 46);
    buildFace(50, 51, 49, 48);
    buildFace(46, 47, 49, 48);
    buildFace(45, 44, 51, 50);
    buildFace(44, 46, 48, 50);
    buildFace(45, 51, 49, 47);

 //Wheel right inner
    buildFace(44, 45, 47, 46);
    buildFace(50, 51, 49, 48);
    buildFace(46, 47, 49, 48);
    buildFace(45, 44, 51, 50);
    buildFace(44, 46, 48, 50);
    buildFace(45, 51, 49, 47);
//Wheel left inner
    buildFace(52, 53, 55, 54);
    buildFace(58, 59, 57, 56);
    buildFace(55, 53, 59, 57);
    buildFace(52, 54, 56, 58);
    buildFace(54, 55, 57, 56);
    buildFace(52, 53, 59, 58);

    //Background black car
    buildFace(60, 61, 63, 62);
    buildFace(64, 65, 67, 66);
    buildFace(62, 63, 67, 66);
    buildFace(60, 61, 65, 64);
    buildFace(60, 64, 66, 62);
    buildFace(61, 65, 67, 63);

    //Tree
    buildFace(68, 69, 71, 70);
    buildFace(72, 73, 75, 74);
    buildFace(70, 71, 75, 74);
    buildFace(68, 69, 73, 72);
    buildFace(68, 72, 74, 70);
    buildFace(69, 73, 75, 71);

    //Leaves
    buildFace(76, 77, 78, 76);
    buildFace(76, 78, 80, 76);
    buildFace(76, 79, 80, 76);
    buildFace(76, 79, 77, 76);
    buildFace(77, 78, 80, 79);

    //Lest House
    buildFace(81, 82, 84, 83);
    buildFace(81, 82, 86, 85);
    buildFace(83, 84, 88, 87);
    buildFace(81, 83, 87, 85);
    buildFace(82, 86, 88, 84);
    buildFace(86, 85, 87, 88);

    //Right House
    buildFace(89, 90, 92, 91);
    buildFace(89, 90, 94, 93);
    buildFace(91, 92, 96, 95);
    buildFace(89, 91, 95, 93);
    buildFace(90, 94, 96, 92);
    buildFace(94, 93, 95, 96);

}


function buildFace(a, b, c, d) {

    var vertices = [
        //Car body
        vec4( -1.7,  -1.84,  1.25, 1.0 ),	// vertex 0
        vec4( -1.5,  -1.00,  1.25, 1.0 ),   // vertex 1
        vec4(  1.5,  -1.00,  1.25, 1.0 ),   // vertex 2
        vec4(  1.7,  -1.84,  1.25, 1.0 ),   // vertex 3
        vec4( -1.7,  -1.84, -0.25, 1.0 ),   // vertex 4
        vec4( -1.5,  -1.00, -0.25, 1.0 ),   // vertex 5
        vec4(  1.5,  -1.00, -0.25, 1.0 ),   // vertex 6
        vec4(  1.7,  -1.84, -0.25, 1.0 ),   // vertex 7


        //Car upper body
        vec4(  0.65, -0.39,-0.22, 1.0 ),    // vertex 8
        vec4(  0.90, -1.0, -0.22, 1.0 ),   // vertex 9
        vec4( -0.90, -1.0, -0.22, 1.0 ),   // vertex 10
        vec4( -0.65, -0.39,-0.22, 1.0 ),   // vertex 12
        vec4(  0.65, -0.39, 1.22, 1.0 ),   // vertex 13
        vec4(  0.90, -1.0,  1.22, 1.0 ),   // vertex 14
        vec4( -0.90, -1.0,  1.22, 1.0 ),   // vertex 15
        vec4( -0.65, -0.39, 1.22, 1.0 ),    // vertex 16


        //Road
        vec4( -4.05, -2.15,  3.00, 1.0),     //vertex 16
        vec4( -4.05, -2.15, -4.55, 1.0),    //vertext 17
        vec4(  4.05, -2.15,  3.00, 1.0),     //vertex 18
        vec4(  4.05, -2.15, -4.55, 1.0),    //vertex 19

        //Windows
        vec4(  0.45, -0.59, -0.23, 1.0 ),    // vertex 20
        vec4(  0.70, -1.00, -0.23, 1.0 ),   // vertex 21
        vec4( -0.70, -1.00, -0.23, 1.0 ),   // vertex 22
        vec4( -0.45, -0.59, -0.23, 1.0 ),   // vertex 23
        vec4(  0.45, -0.59,  1.23, 1.0 ),   // vertex 24
        vec4(  0.70, -1.00,  1.23, 1.0 ),   // vertex 25
        vec4( -0.70, -1.00,  1.23, 1.0 ),   // vertex 26
        vec4( -0.45, -0.59,  1.23, 1.0 ),    // vertex 27

        //Wheel right outer
        vec4(  1.25,  -1.84,  1.25, 1.0 ),   // vertex 28
        vec4(  0.75,  -1.84,  1.25, 1.0 ),   // vertex 29
        vec4(  1.25,  -1.84,  1.20, 1.0 ),   // vertex 30
        vec4(  0.75,  -1.84,  1.20, 1.0 ),   // vertex 31
        vec4(  1.25,  -2.14,  1.20, 1.0 ),   // vertex 32
        vec4(  0.75,  -2.14,  1.20, 1.0 ),   // vertex 33
        vec4(  1.25,  -2.14,  1.25, 1.0 ),   // vertex 34
        vec4(  0.75,  -2.14,  1.25, 1.0 ),   // vertex 35

        //Wheel left outer
        vec4(  -1.25,  -1.84,  1.25, 1.0 ),   // vertex 36
        vec4(  -0.75,  -1.84,  1.25, 1.0 ),   // vertex 37
        vec4(  -1.25,  -1.84,  1.20, 1.0 ),   // vertex 38
        vec4(  -0.75,  -1.84,  1.20, 1.0 ),   // vertex 39
        vec4(  -1.25,  -2.14,  1.20, 1.0 ),   // vertex 40
        vec4(  -0.75,  -2.14,  1.20, 1.0 ),   // vertex 41
        vec4(  -1.25,  -2.14,  1.25, 1.0 ),   // vertex 42
        vec4(  -0.75,  -2.14,  1.25, 1.0 ),   // vertex 43

        //Wheel right inner
        vec4(  1.25,  -1.84,  -0.25, 1.0 ),   // vertex 44
        vec4(  0.75,  -1.84,  -0.25, 1.0 ),   // vertex 45
        vec4(  1.25,  -1.84,  -0.20, 1.0 ),   // vertex 46
        vec4(  0.75,  -1.84,  -0.20, 1.0 ),   // vertex 47
        vec4(  1.25,  -2.14,  -0.20, 1.0 ),   // vertex 48
        vec4(  0.75,  -2.14,  -0.20, 1.0 ),   // vertex 49
        vec4(  1.25,  -2.14,  -0.25, 1.0 ),   // vertex 50
        vec4(  0.75,  -2.14,  -0.25, 1.0 ),   // vertex 51

        //Wheel left inner
        vec4(  -1.25,  -1.84,  -0.25, 1.0 ),   // vertex 52
        vec4(  -0.75,  -1.84,  -0.25, 1.0 ),   // vertex 53
        vec4(  -1.25,  -1.84,  -0.20, 1.0 ),   // vertex 54
        vec4(  -0.75,  -1.84,  -0.20, 1.0 ),   // vertex 55
        vec4(  -1.25,  -2.14,  -0.20, 1.0 ),   // vertex 56
        vec4(  -0.75,  -2.14,  -0.20, 1.0 ),   // vertex 57
        vec4(  -1.25,  -2.14,  -0.25, 1.0 ),   // vertex 58
        vec4(  -0.75,  -2.14,  -0.25, 1.0 ),   // vertex 59

        //Car at the background
        vec4(  -2.95,  -2.14,  -2.0, 1.0 ),   // vertex 60
        vec4(  -1.75,  -2.14,  -2.0, 1.0 ),   // vertex 61
        vec4(  -2.95,  -1.19,  -2.0, 1.0 ),   // vertex 62
        vec4(  -1.75,  -1.19,  -2.0, 1.0 ),   // vertex 63
        vec4(  -2.95,  -2.14,  -3.0, 1.0 ),   // vertex 64
        vec4(  -1.75,  -2.14,  -3.0, 1.0 ),   // vertex 65
        vec4(  -2.95,  -1.19,  -3.0, 1.0 ),   // vertex 66
        vec4(  -1.75,  -1.19,  -3.0, 1.0 ),   // vertex 67
        
        //Tree
        vec4( -1.0, -2.14, -2.5, 1.0 ),   // vertex 68
        vec4(  0.0, -2.14, -2.5, 1.0 ),   // vertex 69
        vec4( -1.0,  -0.5,  -2.5, 1.0 ),   // vertex 70
        vec4(  0.0,  -0.5,  -2.5, 1.0 ),   // vertex 71
        vec4( -1.0, -2.14, -3.5, 1.0 ),   // vertex 72
        vec4(  0.0, -2.14, -3.5, 1.0 ),   // vertex 73
        vec4( -1.0,  -0.5,  -3.5, 1.0 ),   // vertex 74
        vec4(  0.0,  -0.5,  -3.5, 1.0 ),   // vertex 75

        //Leaves

        vec4( -0.5,  1.75,  -3.0, 1.0 ),   // vertex 76
        vec4( -1.5,  -0.5,  -2.0, 1.0 ),   // vertex 77
        vec4(  0.5,  -0.5,  -2.0, 1.0 ),   // vertex 78
        vec4( -1.5,  -0.5,  -4.0, 1.0 ),   // vertex 79
        vec4(  0.5,  -0.5,  -4.0, 1.0 ),   // vertex 80

        //Left House
        vec4(  -4.05,  2.14,  -3.5, 1.0 ),   // vertex 81
        vec4(  -0.5,   2.14,  -3.5, 1.0 ),   // vertex 82
        vec4(  -4.05, -2.14,  -3.5, 1.0 ),   // vertex 83
        vec4(  -0.5,  -2.14,  -3.5, 1.0 ),   // vertex 84
        vec4(  -4.05,  2.14,  -3.5, 1.0 ),   // vertex 85
        vec4(  -0.5,   2.14,  -3.5, 1.0 ),   // vertex 86
        vec4(  -4.05, -2.14,  -3.5, 1.0 ),   // vertex 87
        vec4(  -0.5,  -2.14,  -3.5, 1.0 ),   // vertex 88

        //Right House
        vec4(  4.05,  2.14,  -3.5, 1.0 ),   // vertex 89
        vec4(  0.5,   2.14,  -3.5, 1.0 ),   // vertex 90
        vec4(  4.05, -2.14,  -3.5, 1.0 ),   // vertex 91
        vec4(  0.5,  -2.14,  -3.5, 1.0 ),   // vertex 92
        vec4(  4.05,  2.14,  -3.5, 1.0 ),   // vertex 93
        vec4(  0.5,   2.14,  -3.5, 1.0 ),   // vertex 94
        vec4(  4.05, -2.14,  -3.5, 1.0 ),   // vertex 95
        vec4(  0.5,  -2.14,  -3.5, 1.0 ),   // vertex 96
    ];

    var faceColors = [
        //Body color
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 1.0, 0.75, 1.0, 1.0 ],  // pink
        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 1.0, 0.75, 1.0, 1.0 ],  // pink
        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 0.0, 0.0, 0.0, 1.0 ],  // black

        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 1.0, 0.75, 1.0, 1.0 ],  // pink
        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 1.0, 0.75, 1.0, 1.0 ],  // pink
        [ 0.0, 1.0, 1.0, 1.0 ],  // cyan
        [ 0.5, 0.25, 0.25, 1.0 ]   // white

        //Road color
        [ 1.0, 1.0, 1.0, 1.0 ],  // white
        [ 1.0, 0.0, 0.0, 1.0 ],  // red        
        [ 0.5, 0.5, 0.3, 1.0 ],  // grey        
        [ 0.0, 0.4, 0.0, 1.0 ],  // green

        //Windows
        [ 0.0, 0.0, 0.0, 1.0 ],  // black      
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 1.0, 1.0, 1.0 ],  // cyan
        [ 1.0, 0.5, 1.0, 1.0 ]   // white
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 1.0, 0.0, 0.0, 1.0 ],  // red
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 1.0, 0.0, 1.0 ],  // green
        
        //Outer Right Wheel
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black

        //Outer Left Wheel
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black

        //Inner Right Wheel
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black

        //Inner Left Wheel
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black

        //Background Car
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black

        // Tree
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black        
        [ 0.0, 0.0, 0.0, 1.0 ],  // black 

        // Leaves             
        [ 0.0, 0.5, 0.0, 0.5 ],  // green
        [ 0.0, 0.5, 0.0, 0.5 ],  // green
        [ 0.0, 0.5, 0.0, 0.5 ],  // green
        [ 0.0, 0.5, 0.0, 0.5 ],  // green        
        [ 0.0, 0.0, 0.0, 1.0 ],  // black

        //Left House
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 0.0, 0.0, 0.0, 0.5 ],  // black

        //Right House
        [ 0.0, 0.0, 0.0, 0.5 ],  // black
        [ 0.0, 0.0, 0.0, 0.5 ],  // black
        [ 0.0, 0.0, 0.0, 0.5 ],  // black
        [ 0.0, 0.0, 0.0, 0.5 ],  // black
        [ 0.0, 0.0, 0.0, 0.5 ],  // black
        [ 0.0, 0.0, 0.0, 0.5 ],  // black
        [ 0.0, 0.0, 0.0, 0.5 ],  // black
        [ 0.0, 0.0, 0.0, 0.5 ],  // black
    ];

    // WebGL renders triangles.  We need to split the cube face into
    // two triangles.  The trick below creates triangle a,b,c then
    // a,c,d from face a,b,c,d.  It then colors the face the color
    // of the first passed in vertice "a".  So don't call this function with
    // the same starting vertice (otherwise you will have faces with the
    // same color)
    //setting up the normal
    var v1 = subtract(vertices[b], vertices[a]);
    var v2 = subtract(vertices[c], vertices[b]);
    var normal = cross(v1, v2);
    normal = vec3(normal);
    console.log(normal);

    var indices = [ a, b, c, a, c, d ];

    for ( var i = 0; i < indices.length; ++i ) {
        
        // Add to array each triangle 
        points.push( vertices[indices[i]] );

        // Color the two triangles the same color        
        colors.push(faceColors[a]);
        normalsArray.push(normal);
        NumVertices +=1;
    }
}

function render() {

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    //gl.clearColor(0.0, 0.0, 1.0, 0.25);
    setViewProject(program);
    updateVariables();
    theta[axis] += 1.0;
    if(!freeze)
        theta[axis] += 1.0;
    else
        theta[axis] = 0.0;
    if (!walk)
        vs_Dist[2] += 0.05;
    else
        vs_Dist[2] = 0.0;
    gl.uniform3fv(thetaLoc, theta);

    gl.drawArrays( gl.TRIANGLES, 0, NumVertices );

    requestAnimFrame( render );
}


function setViewProject(program){
    VMatrixLoc = gl.getUniformLocation(program, "vs_ViewMatrix");
    PMatrixLoc = gl.getUniformLocation(program, "vs_ProjMatrix");

    if (!VMatrixLoc || !PMatrixLoc){
        console.log('Failed to locate vs_ViewMatrix or vs_ProjMatrix');
        return;
    }
    upVec = vec3(upX, upY, upZ);
    eyePt = vec3(eyeX, eyeY, eyeZ);
    atPt = vec3(centerX, centerY, centerZ);
    vs_ViewMatrix = lookAt(eyePt, atPt, upVec);
    aspect = canvas.width/canvas.height;
    vs_ProjMatrix = perspective(fovy, aspect, near, far);
    gl.uniformMatrix4fv(VMatrixLoc, false, flatten(vs_ViewMatrix));
    gl.uniformMatrix4fv(PMatrixLoc, false, flatten(vs_ProjMatrix));
}

function updateVariables(){
    document.getElementById("fov").innerHTML = '<strong>Field of View (' +fovy+ ')</strong>';
    document.getElementById("eye").innerHTML = '<strong>Eye (' +eyeX+ ', '+eyeY+', '+eyeZ+')</strong>';
    document.getElementById("center").innerHTML = '<strong>Center (' +centerX+ ', '+centerY+', '+centerZ+')</strong>';
    document.getElementById("up").innerHTML = '<strong>Up (' +upX+ ', '+upY+', '+upZ+')</strong>';
}

function loadPhongModel(){
    //Sets the phong lighting model
    var ambientProduct = mult(lightAmbient, materialAmbient);
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);
    var specularProduct = mult(lightSpecular, materialSpecular);

    gl.uniform4fv( gl.getUniformLocation(program, 
       "vs_AmbientProduct"),flatten(ambientProduct) );
    gl.uniform4fv( gl.getUniformLocation(program, 
       "vs_DiffuseProduct"),flatten(diffuseProduct) );
    gl.uniform4fv( gl.getUniformLocation(program, 
       "vs_SpecularProduct"),flatten(specularProduct) );    
    gl.uniform4fv( gl.getUniformLocation(program, 
       "vs_LightPosition"),flatten(lightPosition) );

    gl.uniform1f( gl.getUniformLocation(program, 
       "vs_Shininess"),materialShininess );
}