#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <semaphore.h>


//Reference: From Textbook and class of Operating System in Programming

#define MAX_CUSTOMERS 5
int count = 0;
sem_t mutex;

void *checkout(void *a)
{
	int i, tmp;
int x;
	x = *((int *) a);
	for(i = 0;i<MAX_CUSTOMERS;i++)
	{
	printf("receiption %d waiting for customer %d \n",x,count);
	sem_wait(&mutex);
		printf("Receiption %d: Now have customer...\n",x);
	tmp = count;
		tmp = tmp + 1;
		count = tmp;
		printf("Receiption %d: Exiting ...\n",x);
		sem_post(&mutex);
	}
	return NULL;
}

int main()
{
	pthread_t thread_a;
	pthread_t thread_b;
	pthread_t thread_c;
	pthread_t thread_d;
	pthread_t thread_e;
	sem_init(&mutex, 0,1);
	int i[5];
	i[0] = 0;
	i[1] = 1;
	i[2] = 2;
	i[3] = 3;
	i[4] = 4;

	pthread_create(&thread_a,NULL,(void*) &checkout,(void*) &i[0]);
	pthread_create(&thread_b,NULL,(void*) &checkout,(void*) &i[1]);
	pthread_create(&thread_c,NULL,(void*) &checkout,(void*) &i[2]);
	pthread_create(&thread_d,NULL,(void*) &checkout,(void*) &i[3]);
	pthread_create(&thread_e,NULL,(void*) &checkout,(void*) &i[4]);

	pthread_join(thread_a,NULL);
	pthread_join(thread_b,NULL);
	pthread_join(thread_c,NULL);
	pthread_join(thread_d,NULL);
	pthread_join(thread_e,NULL);

	sem_destroy(&mutex);
	pthread_exit(NULL);
}



